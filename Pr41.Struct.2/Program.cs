﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pr41.Struct._2
{
    struct File
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public FileType Type { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastModify { get; set; }
        public FileAccess Access { get; set; }


        public static File FromString(string line)
        {
            var file = new File();
            var parsed = line.Split(',');
            file.Name = parsed[0];
            file.Size = int.Parse(parsed[1]);
            file.Type = parsed[2] switch
            {
                "изображение" => FileType.Image,
                "аудио" => FileType.Audio,
                "видео" => FileType.Video,
                "презентация" => FileType.Presentation,
                "электронная таблица" => FileType.Table,
                "теучтовый" => FileType.Text,
                _ => FileType.Text
            };
            
            file.Created = DateTime.Parse(parsed[3]);
            file.LastModify = DateTime.Parse(parsed[4]);
            file.Access = parsed[5] switch
            {
                "ограничен" => FileAccess.Limited,
                "закрыт" => FileAccess.Close,
                "открыт всем" => FileAccess.Open,
                _ => FileAccess.Close
            };

            return file;
        }

        public override string ToString()
        {
            return $"Имя: {Name}, Размер: {Size}, Тип: {Type}," +
                   $" Дата создания: {Created}, Дата последней модификации: {LastModify}, " +
                   $"Уровень доступа: {Access}";
        }
    }

    enum FileType
    {
        Audio,
        Video,
        Image,
        Presentation,
        Text,
        Table
    }

    enum FileAccess
    {
        Open,
        Close,
        Limited
    }

    class Program
    {
        static void Main(string[] args)
        {
            var files = new List<File>();
            try
            {
                using var reader = new StreamReader("files.txt");

                var line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    var file = File.FromString(line);
                    files.Add(file);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine($"Количество типа аудио: {files.Count(file => file.Type == FileType.Audio)}");
            Console.WriteLine($"Количество типа виде: {files.Count(file => file.Type == FileType.Video)}");
            Console.WriteLine($"Количество типа изображение: {files.Count(file => file.Type == FileType.Image)}");
            Console.WriteLine($"Количество типа презентация: {files.Count(file => file.Type == FileType.Presentation)}");
            Console.WriteLine($"Количество типа электронная таблица: {files.Count(file => file.Type == FileType.Table)}");
            Console.WriteLine($"Количество типа текстовый: {files.Count(file => file.Type == FileType.Text)}");

            Console.WriteLine($"Презентации ограниченного доступа, которые изменялись в 2012 году: ");
            foreach (var file in files.Where(file => file.Type == FileType.Presentation && file.Access == FileAccess.Limited && file.LastModify.Year == 2012))
            {
                Console.WriteLine(file.ToString());
            }
            
            Console.WriteLine($"Видео размером больше 100 Мбайт, созданных во второй половине 2011 года: ");
            foreach (var file in files.Where(file => file.Type == FileType.Video && file.Size > 100*1000 && file.Created.Year == 2011 && file.Created.Month >= 6))
            {
                Console.WriteLine(file.ToString());
            }
        }
    }
}