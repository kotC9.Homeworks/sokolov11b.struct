﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Pr41.Struct._1
{
    public struct Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public Marks StudentMarks { get; set; }

        public static Student FromString(string input)
        {
            var student = new Student();
            var parsedInput = input.Split(',');
            student.Name = parsedInput[0];
            student.Surname = parsedInput[1];

            var marks = new Marks
            {
                Math = int.Parse(parsedInput[2]),
                Rus = int.Parse(parsedInput[3]),
                Physics = int.Parse(parsedInput[4]),
                History = int.Parse(parsedInput[5])
            };

            student.StudentMarks = marks;
            return student;
        }
    }

    public struct Marks
    {
        public int Math { get; set; }
        public int Rus { get; set; }
        public int Physics { get; set; }
        public int History { get; set; }

        public int Sum()
        {
            return Math + Rus + Physics + History;
        }

        public bool AnyBadMark()
        {
            return Math == 2 || Rus == 2 || Physics == 2 || History == 2;
        }
    }
    
    class Program
    {
        static void Main(string[] args)
        {
            var students = new List<Student>();
            try
            {
                using var reader = new StreamReader("marks.txt");

                var line = "";
                while ((line = reader.ReadLine()) != null)
                {
                    var student = Student.FromString(line);
                    students.Add(student);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            // write statistics
            
            Console.WriteLine("Средние баллы: ");
            Console.WriteLine($"Средний бал по математике: {students.Average(student => student.StudentMarks.Math)}");
            Console.WriteLine($"Средний бал по русскому: {students.Average(student => student.StudentMarks.Rus)}");
            Console.WriteLine($"Средний бал по физике: {students.Average(student => student.StudentMarks.Physics)}");
            Console.WriteLine($"Средний бал по истории: {students.Average(student => student.StudentMarks.History)}");
            
            Console.WriteLine($"Максимальная сумма баллов: {students.Max(student => student.StudentMarks.Sum())}");
            Console.WriteLine($"Количество учащийся, получивших хотя бы одну двойку: {students.Count(student => student.StudentMarks.AnyBadMark())}");
        }
    }
}
